
public class App 
{
    public static void main( String[] args )
    {
        System.out.println();
        Bird birdday = new Bird("Birdday");
        birdday.eat();
        birdday.sleep();
        birdday.takeoff();
        birdday.fly();
        birdday.landing();
        System.out.println();

        Plane FightingFalcon = new Plane("F-16", "FightingFalcon");
        FightingFalcon.takeoff();
        FightingFalcon.fly();
        FightingFalcon.landing();
        System.out.println();

        Superman Clark = new Superman("Clark");
        Clark.takeoff();
        Clark.fly();
        Clark.landing();
        Clark.eat();
        Clark.sleep();
        Clark.walk();
        Clark.run();
        Human man1 = new Human("holykung");
        man1.eat();
        man1.sleep();
        man1.walk();
        man1.run();
        man1.swim();
        System.out.println();

        Submarine Seawolf = new Submarine("Seawolf", "Diesel");
        Bat Vampirebat = new Bat("Vampire bat");
        Snake blackmamba = new Snake("black mamba");
        Rat jerry = new Rat("Jerry");
        Dog Odie = new Dog("Odie");
        Cat Garfield = new Cat("Garfield");
        Fish Salmon = new Fish("Salmon");
        Crocodile Caiman = new Crocodile("Caiman");
        Flyable[] flyables = {birdday,FightingFalcon,Clark,Vampirebat};
        for (int i = 0; i < flyables.length; i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }
        System.out.println();
        Walkable[] walkables = {birdday,Clark,man1,jerry,Odie,Garfield,Caiman};
        for (int i = 0; i < walkables.length; i++) {
            walkables[i].walk();
            walkables[i].run();
        }
        System.out.println();
        Swimable[] swimables = {Clark,man1,Seawolf,jerry,Odie,Garfield,Caiman,Salmon};
        for (int i = 0; i < swimables.length; i++) {
            swimables[i].swim();
        }
        System.out.println();
        Crawlable[] crawlables = {blackmamba,jerry,Odie,Garfield,Caiman};
        for (int i = 0; i < crawlables.length; i++) {
            crawlables[i].crawl();
        }
    }
}

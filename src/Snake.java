

public class Snake extends Animal implements Crawlable {

    public Snake(String name) {
        super(name, 0);
        //TODO Auto-generated constructor stub
    }

    @Override
    public void crawl() {
        System.out.println(this.toString() + " crawl.");
        
    }

    @Override
    public void sleep() {
        System.out.println(this + " sleep.");
    }

    @Override
    public void eat() {
        System.out.println(this + " eat.");
        
    }
    @Override
    public String toString (){
        return "Snake (" + this.getName() + ")";
    }
}
